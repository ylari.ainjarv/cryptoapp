package ee.bcs.valiit;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Decryptor extends Cryptor {

    public Decryptor(String filePath) throws IOException {
        super(filePath);
    }

    @Override
    protected Map<String, String> generateDictionary(List<String> fileLines) {
        Map<String, String> dictionary = new HashMap<>();
        for (String dictLine: fileLines) {
            String[] lineParts = dictLine.split(", ");
            dictionary.put(lineParts[1], lineParts[0]);
        }
        return dictionary;
    }

}
