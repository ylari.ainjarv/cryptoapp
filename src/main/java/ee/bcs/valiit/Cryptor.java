package ee.bcs.valiit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> dictionary = null;

    public Cryptor(String filePath) throws IOException {
        List<String> fileLines = readAlphabet(filePath);
        this.dictionary = generateDictionary(fileLines);
    }

    protected List<String> readAlphabet(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return Files.readAllLines(path);
    }

    protected abstract Map<String, String> generateDictionary(List<String> fileLines);

    public String translate(String text) {
        if (text != null) {
            StringBuilder result = new StringBuilder();
            char[] textChars = text.toCharArray();
            for (char c: textChars) {
                String letter = String.valueOf(c).toUpperCase();
                // Lahendus 1
                String sym = dictionary.get(letter);
                result.append(sym != null ? sym : letter);
                // Lahendus 2
                // result.append(dictionary.containsKey(letter) ? dictionary.get(letter) : letter);
            }
            return result.toString();
        }
        return null;
    }

}
